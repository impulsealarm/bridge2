/*
 * Impulse Alarm Bridge V2.0
 * Last revision 02/18/2016
 * Description: Widget My Account Manager
 */

impulseBridge.controller('myAccountManagerController', ['$scope', '$filter', '$sce', '$http', '$mdSidenav', '$mdDialog', '$mdMedia',
    function ($scope, $filter, $sce, $http, $mdSidenav, $mdDialog, $mdMedia) {
        $scope.editingLoaded = false;
    $scope.editYourInfo = function(ev) {
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
        $mdDialog.show({
                controller: EditInfoController,
                templateUrl: 'views/dashboards/dialogs/edit.info.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: useFullScreen
            })
            .then(function() {
                $scope.message = 'Message is sent.';
            }, function() {

            });
        $scope.$watch(function() {
            return $mdMedia('xs') || $mdMedia('sm');
        }, function(wantsFullScreen) {
            $scope.customFullscreen = (wantsFullScreen === true);
        });

    };
}]);

function EditInfoController($scope, $http, $mdDialog) {
    $scope.editingLoading = true;
    $scope.editingLoaded = false;
    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    $http({
        method: 'GET',
        url: 'https://impulsealarm.com/xapi/realtor_dashboard/'
    }).then(function success(response) {
        $scope.editingLoading = false;
        $scope.affData = response.data;
        //realtor data
        $scope.fName = $scope.affData["fname"];
        $scope.lName = $scope.affData["lname"];
        $scope.email = $scope.affData["email"];
        $scope.phone = $scope.affData["phone"];
        $scope.avatr = $scope.affData["avatr"];
        $scope.editingLoaded = true;
    }, function failure(response) {
    });
}