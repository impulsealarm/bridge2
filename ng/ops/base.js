/*
 * Impulse Alarm Bridge V2.0
 * Last revision 02/18/2016
 * Description: Base JS script containing core definitions of Angular app module, also main jQuery codes.
 */

$( document ).ready(function() {

});


var impulseBridge = angular.module('impulseBridge', [
    'ngAnimate',
    'ngSanitize',
    'ngRoute',
    'ngMaterial',
    'ngAria',    
    'ngMessages',
    'ngCookies',
    'ui.bootstrap']).run();

angular.module('impulseBridge')
    .config(function($httpProvider) {
        $httpProvider.defaults.headers.post['Content-Type'] =  'application/x-www-form-urlencoded';
    })
    .service('apiImpulseBridgeLoginStatus', function($http) {
        this.getUserStatus = function() {
            return $http({
                method: 'GET',
                url: 'https://impulsealarm.com/get_login_status/'
            })
        }
    })
impulseBridge.run(['$location', '$rootScope', '$http', function($location, $rootScope, $http) {
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        if (current.hasOwnProperty('$$route')) {
            $rootScope.title = current.$$route.title;
        }
    });
}]);







