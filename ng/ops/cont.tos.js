/*
 * Impulse Alarm Bridge V2.0
 * Last revision 02/23/2016
 * Description: Terms of Service page.
 */

impulseBridge.controller('tosController', ['$scope', '$filter', '$sce', '$http', '$mdDialog', '$mdMedia', '$route',
    function ($scope, $filter, $sce, $http, $mdDialog, $mdMedia, $route) {
        $scope.signed = false;
        $scope.notsigned = false;

        $http({
            method: 'GET',
            url: 'https://impulsealarm.com/xapi/get_login_status'
        }).then(function successCallback(response) {
            $scope.status = response.data;
            //user data
            $scope.user = $scope.status.name;
            $scope.email = $scope.status.email;

            var logStatus = $scope.status.status;
            var tosStatus = $scope.status.tos;

            if(logStatus == "loggedin") {
                if (tosStatus != "checked") {
                    $scope.notsigned = true;
                    $scope.signed = false;
                    var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
                    $mdDialog.show({
                            controller: TourDialog,
                            templateUrl: 'views/dashboards/dialogs/tour.dialog.html',
                            parent: angular.element(document.body),

                            clickOutsideToClose:true,
                            fullscreen: useFullScreen
                        })
                        .then(function() {
                            $scope.message = 'Message is sent.';
                        }, function() {

                        });
                    $scope.acceptTos = function() {
                        params = {
                            "tos": "checked"
                        }
                        $http({
                            method: 'POST',
                            url: "https://impulsealarm.com/xapi/requests/",
                            params: params
                        }).then(function (response) {
                            setTimeout(function() {
                                $route.reload();
                            }, 1500)
                        }, function (response) {})
                    }
                } else {
                    $scope.notsigned = false;
                    $scope.signed = true;
                    setTimeout(function() {
                        window.location.assign("#/realtor/dashboard");
                    }, 1500);
                }
            } else {
                window.location.assign("#/start");
            }
        }, function errorCallback(response) {
            console.log(response.data);
        });
    }]);

function TourDialog($scope, $mdDialog) {
    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };

}