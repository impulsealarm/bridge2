/*
 * Impulse Alarm Bridge V2.0
 * Last revision 02/23/2016
 * Description: Main router of the app
 */
impulseBridge.config(function ($routeProvider) {
    $routeProvider
        .when('/start',
            {
                controller: 'mainController',
                templateUrl: 'views/pg.main.html',
                title: 'Start - Bridge'
            })
        .when('/customer',
            {
                controller: 'customerController',
                templateUrl: 'views/pg.customer.html',
                title: 'Customer - Bridge'
            })
        .when('/realtor/dashboard',
            {
                controller: 'realtorDashController',
                templateUrl: 'views/dashboards/dash.realtor.html',
                title: 'Realtor - Dashboard - Bridge'
            })
        .when('/tos',
            {
                controller: 'tosController',
                templateUrl: 'views/pg.tos.html',
                title: 'Our Terms of Service - Bridge'
            })
        .otherwise({ redirectTo: '/start' });
});