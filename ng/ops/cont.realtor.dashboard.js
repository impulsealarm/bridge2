/*
 * Impulse Alarm Bridge V2.0
 * Last revision 022/23/2016
 * Description: Realtor Dashboard controller
 */

impulseBridge.controller('realtorDashController', ['$scope', '$filter', '$sce', '$http', '$mdDialog', '$mdMedia', '$window',
    function ($scope, $filter, $sce, $http, $mdDialog, $mdMedia, $window) {
        $scope.allowed = false;
        $scope.loadingStuff = true;
        $scope.stuffBeingLoaded = false;
        this.isOpen = false;
        $scope.openIt = function(what) {
          window.location.href = what;
        }
        $http({
            method: 'GET',
            url: 'https://impulsealarm.com/xapi/get_login_status'
        }).then(function successCallback(response) {
            $scope.status = response.data;
            //user data
            $scope.user = $scope.status.name;
            $scope.email = $scope.status.email;

            var logStatus = $scope.status.status;
            var tosStatus = $scope.status.tos;
            var contStatus = $scope.status.cont;
            if(logStatus == "loggedin") {
                if (tosStatus != "checked") {
                    window.location.assign("#/tos");
                } else {
                    if (contStatus != "signed") {
                        setTimeout(function() {
                            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
                            $mdDialog.show({
                                    controller: DialogController,
                                    templateUrl: 'views/dashboards/dialogs/tour2.dialog.html',
                                    parent: angular.element(document.body),
                                    clickOutsideToClose:true,
                                    fullscreen: useFullScreen
                                })
                                .then(function() {
                                    $scope.message = 'Message is sent.';
                                }, function() {

                                });
                            $scope.$watch(function() {
                                return $mdMedia('xs') || $mdMedia('sm');
                            }, function(wantsFullScreen) {
                                $scope.customFullscreen = (wantsFullScreen === true);
                            });
                        }, 1500)
                    } else {

                    }
                    $scope.allowed = true;
                    $http({
                        method: 'GET',
                        url: 'https://impulsealarm.com/xapi/realtor_dashboard/'
                    }).then(function success(response) {

                        //all data
                        $scope.affData = response.data;

                        //leads data
                        $scope.leads = $scope.affData["leads"];

                        //contacts data
                        $scope.conts = $scope.affData["conts"];

                        //realtor data
                        $scope.fName = $scope.affData["fname"];
                        $scope.lName = $scope.affData["lname"];
                        $scope.email = $scope.affData["email"];
                        $scope.phone = $scope.affData["phone"];
                        $scope.avatr = $scope.affData["avatr"];

                        //manager data
                        $scope.mgrfn = $scope.affData["mgrfn"];
                        $scope.mgrln = $scope.affData["mgrln"];
                        $scope.mgrem = $scope.affData["mgrem"];
                        $scope.mgrph = $scope.affData["mgrph"];

                        //page handling
                        $scope.loadingStuff = false;
                        $scope.stuffBeingLoaded = true;

                    }, function failure(response) {
                        //no data response
                    })
                }
            } else {
                window.location.assign("#/start");
            }
        }, function errorCallback(response) {
            console.log(response.data);
        });
}]);

impulseBridge.controller('sendEmailDashboardController', ['$scope', '$filter', '$sce', '$http', '$mdSidenav', '$mdDialog', '$mdMedia',
    function ($scope, $filter, $sce, $http, $mdSidenav, $mdDialog, $mdMedia) {

        $scope.emailDialog = function(ev) {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
            $mdDialog.show({
                    controller: EmailRefController,
                    templateUrl: 'views/dashboards/dialogs/email.ref.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: useFullScreen
                })
                .then(function() {
                    $scope.message = 'Message is sent.';
                }, function() {

                });
            $scope.$watch(function() {
                return $mdMedia('xs') || $mdMedia('sm');
            }, function(wantsFullScreen) {
                $scope.customFullscreen = (wantsFullScreen === true);
            });
        };

    }]);

impulseBridge.controller('downloadFlyersDashboardController', ['$scope', '$filter', '$sce', '$http', '$mdSidenav', '$mdDialog', '$mdMedia',
    function ($scope, $filter, $sce, $http, $mdSidenav, $mdDialog, $mdMedia) {

        $scope.flyerDialog = function(ev) {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
            $mdDialog.show({
                    controller: FlyerRefController,
                    templateUrl: 'views/dashboards/dialogs/flyers.ref.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: useFullScreen
                })
                .then(function() {
                    $scope.message = 'Message is sent.';
                }, function() {

                });
            $scope.$watch(function() {
                return $mdMedia('xs') || $mdMedia('sm');
            }, function(wantsFullScreen) {
                $scope.customFullscreen = (wantsFullScreen === true);
            });
        };

    }]);

impulseBridge.controller('myReferralGiftsController', ['$scope', '$filter', '$sce', '$http', '$mdSidenav', '$mdDialog', '$mdMedia',
    function ($scope, $filter, $sce, $http, $mdSidenav, $mdDialog, $mdMedia) {
        $http({
            method: 'GET',
            url: 'https://impulsealarm.com/xapi/realtor_dashboard/'
        }).then(function success(response) {

            //all data
            $scope.affData = response.data;

            //realtor data
            $scope.link1 = $scope.affData["link1"];
            $scope.link2 = $scope.affData["link2"];
            $scope.link3 = $scope.affData["link3"];

        }, function failure(response) {
            //no data response
        })
    }]);


impulseBridge.controller('addNewReferral', ['$scope', '$filter', '$sce', '$http', '$mdSidenav',
    function ($scope, $filter, $sce, $http, $mdSidenav) {
        $scope.addingNewRef = false;
        $scope.addNewRef = function () {
            $scope.addingNewRef = true;
            $http({
                method: 'GET',
                url: 'https://impulsealarm.com/xapi/realtor_dashboard/'
            }).then(function successCallback(response) {
                $scope.affStuff = response.data;
                $scope.rid = $scope.affStuff["rid"];
                $scope.oid = $scope.affStuff["mgrid"];
                console.log($scope.rid);
                params = {
                    "fname": $scope.fname,
                    "lname": $scope.lname,
                    "email": $scope.email,
                    "phone": $scope.phone,
                    "rid": $scope.rid,
                    "oid": $scope.oid
                }
                $http({
                    method: 'POST',
                    url: 'https://impulsealarm.com/xapi/add-new-lead/',
                    params: params
                }).then(function successCallback() {
                    console.log("added to database");
                    $scope.addingNewRef = false;
                    window.location.assign("https://impulsealarm.com/bridge2/");
                }, function errorCallback() {})

            }, function errorCallback(response) {})
        }
    }]);

impulseBridge.controller('myDealController', ['$scope', '$filter', '$sce', '$http', '$mdSidenav',
    function ($scope, $filter, $sce, $http, $mdSidenav) {

    }]);

impulseBridge.controller('myPaymentInfoController', ['$scope', '$filter', '$sce', '$http', '$mdSidenav', '$mdDialog', '$mdMedia',
    function ($scope, $filter, $sce, $http, $mdSidenav, $mdDialog, $mdMedia) {
        $scope.showAddPaymentDialog = function(ev) {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
            $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'views/dashboards/dialogs/payment.info.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: useFullScreen
                })
                .then(function() {
                    $scope.message = 'Message is sent.';
                }, function() {

                });
            $scope.$watch(function() {
                return $mdMedia('xs') || $mdMedia('sm');
            }, function(wantsFullScreen) {
                $scope.customFullscreen = (wantsFullScreen === true);
            });
        };
    }]);

impulseBridge.controller('myContractInfoController', ['$scope', '$filter', '$sce', '$http', '$mdSidenav', '$mdDialog', '$mdMedia',
    function ($scope, $filter, $sce, $http, $mdSidenav, $mdDialog, $mdMedia) {
        $scope.showSignContractDialog = function(ev) {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
            $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'views/dashboards/dialogs/contract.info.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: useFullScreen
                })
                .then(function() {
                    $scope.message = 'Message is sent.';
                }, function() {

                });
            $scope.$watch(function() {
                return $mdMedia('xs') || $mdMedia('sm');
            }, function(wantsFullScreen) {
                $scope.customFullscreen = (wantsFullScreen === true);
            });
        };

    }]);

impulseBridge.controller('fabAddNew', ['$scope', '$filter', '$sce', '$http', '$mdSidenav', '$mdDialog', '$mdMedia',
    function ($scope, $filter, $sce, $http, $mdSidenav, $mdDialog, $mdMedia) {
        $scope.addNewData = function(ev) {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
            $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'views/dashboards/dialogs/add.new.data.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: useFullScreen
                })
                .then(function() {
                    $scope.message = 'Message is sent.';
                }, function() {

                });
            $scope.$watch(function() {
                return $mdMedia('xs') || $mdMedia('sm');
            }, function(wantsFullScreen) {
                $scope.customFullscreen = (wantsFullScreen === true);
            });
        };
    }]);

function DialogController($scope, $mdDialog) {
    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    $scope.answer = function(answer) {
        $mdDialog.hide(answer);
    };
}
function EmailRefController($scope, $http, $mdDialog) {
    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    $scope.loadReferrals = function() {
        $http({
            method: 'GET',
            url: 'https://impulsealarm.com/xapi/realtor_dashboard/'
        }).then(function success(response) {
            $scope.affData = response.data;
            //leads data
            $scope.leads = $scope.affData["leads"];
        }, function failure(response) {
            //no data response
        });
        console.log("clicked");
    };
}

function FlyerRefController($scope, $http, $mdDialog) {
    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    $scope.loadReferrals = function() {
        $http({
            method: 'GET',
            url: 'https://impulsealarm.com/xapi/realtor_dashboard/'
        }).then(function success(response) {
            $scope.affData = response.data;
            //leads data
            $scope.leads = $scope.affData["leads"];
        }, function failure(response) {
            //no data response
        });
        console.log("clicked");
    };
}
